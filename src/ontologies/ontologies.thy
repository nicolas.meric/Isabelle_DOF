(*************************************************************************
 * Copyright (C) 
 *               2019      The University of Exeter 
 *               2018-2019 The University of Paris-Saclay
 *               2018      The University of Sheffield
 *
 * License:
 *   This program can be redistributed and/or modified under the terms
 *   of the 2-clause BSD-style license.
 *
 *   SPDX-License-Identifier: BSD-2-Clause
 *************************************************************************)

theory 
  ontologies
imports
  "CENELEC_50128/CENELEC_50128"
  "Conceptual/Conceptual"
  "scholarly_paper/scholarly_paper"
  "small_math/small_math"
  "technical_report/technical_report"
  (* "CC_v3_1_R5/CC_v3_1_R5" *)
begin
end
